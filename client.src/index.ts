// import express from 'express';
import WebSocket from 'ws';
import handleInput from './handleInput';
import * as routines from './routines';

const startServer = () => new Promise<WebSocket>((resolve, reject) => {

  const ws = new WebSocket('ws://127.0.0.1:9090');

  // const app = express();

  ws.on('open', () => {
    resolve(ws);
  });

  ws.on('error', (error) => {
    reject(error);
  });

  ws.on('close', () => {
    console.log('Server connection Closed');
  });

});

startServer().then((ws) => {
  // routines.PlatformClosed(ws);
  // routines.TokenRequest(ws);
  // handleInput(ws);
});

export default startServer;
