import uuid from 'uuid';
import WebSocket from 'ws';
import { ClientMessages } from '../../../common/clientMessages';
import * as interfaces from '../../../common/interfaces';

interface LogoutRequest {
  UserId: string;
  JsonWebToken: string;
}

export const ClientLogout = (ws: WebSocket, data: LogoutRequest) => {

  const response: interfaces.ClientLogin.ClientLoginResponse = {
    JsonWebToken: data.JsonWebToken,
    Msg: ClientMessages.ClientLogout,
    PasswordReset: false,
    Sender: 23,
    UUID: uuid(),
    UserId: data.UserId,
  };

  ws.send(JSON.stringify(response));

  console.log(response.Msg);
};
