import uuid from 'uuid';
import WebSocket from 'ws';
import { ClientMessages } from '../../../common/clientMessages';
import * as interfaces from '../../../common/interfaces';

interface LoginRequest {
  Secret: string;
  Uuid: string;
}

export const ClientLoginRequest = (ws: WebSocket, data: LoginRequest) => {

  const response: interfaces.ClientLogin.ClientLoginRequest = {
    Msg: ClientMessages.ClientLoginRequest,
    RequestPattern: ClientMessages.ClientLoginRequest,
    Secret: uuid(),
    Sender: 23,
    Time: 1570644790666,
    Uuid: data.Uuid,
  };

  console.log(response.Msg);
  return ws.send(JSON.stringify(response));
};
