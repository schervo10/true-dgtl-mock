import uuid from 'uuid';
import WebSocket from 'ws';
import { ClientMessages } from '../../../common/clientMessages';
import * as interfaces from '../../../common/interfaces';

interface LoginRequest {
  Password: string;
  UserId: string;
}

export const ClientLogin = (ws: WebSocket, data: LoginRequest) => {

  const response: interfaces.ClientLogin.ClientLoginResponse = {
    JsonWebToken: uuid(),
    Msg: ClientMessages.ClientLogin,
    PasswordReset: false,
    Sender: 23,
    UUID: uuid(),
    UserId: data.UserId,
  };

  console.log(response.Msg);
  return ws.send(JSON.stringify(response));
};
