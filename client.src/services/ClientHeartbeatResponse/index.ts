import WebSocket from 'ws';
import { ClientMessages } from '../../../common/clientMessages';
import * as interfaces from '../../../common/interfaces';

export const ClientHeartbeatResponse = (ws: WebSocket, data: interfaces.ClientHeartbeat.ClientHeartbeatRequest) => {

  const response: interfaces.ClientHeartbeat.ClientHeartbeatResponse = {
    JsonWebToken: 'asdjbasdj',
    Msg: ClientMessages.ClientHeartbeatResponse,
    SeqNum: 384,
    Time: 1569937039741,
    UUID: data.UUID,
    UserId: 'MM1_CLOBUI',
  };

  ws.send(JSON.stringify(response));

  console.log(response.Msg);

};
