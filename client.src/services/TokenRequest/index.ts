import uuid from 'uuid';
import WebSocket from 'ws';
import { ClientMessages } from '../../../common/clientMessages';
import * as interfaces from '../../../common/interfaces';

interface TokenRequestType {
  SourceIp: string;
}

export const TokenRequest = (ws: WebSocket, data: TokenRequestType) => {

  const response: interfaces.Token.TokenRequest = {
    Msg: ClientMessages.TokenRequest,
    RequestPattern: ClientMessages.TokenRequest,
    SourceIp: data.SourceIp,
    Time: 0,
    Uuid: uuid(),
  };

  ws.send(JSON.stringify(response));

  console.log(response.Msg);
};
