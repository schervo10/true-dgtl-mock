import uuid from 'uuid';
import WebSocket from 'ws';
import { OrderStatus } from '../../common/enums/OrderStatus';
import * as interfaces from '../../common/interfaces';
import { ServerMessages } from '../../common/serverMessages';
import startServer from '../index';
import * as Services from '../services';

describe('LOGIN', () => {

  let ws: WebSocket = null;

  beforeAll(async () => {
    ws = await startServer();
  });

  it(ServerMessages.ClientLoginResponse, (done) => {
    expect.assertions(3);

    const request = {
      Password: 'Testing123',
      UUID: uuid(),
      UserId: 'USER_1',
    };

    ws.on('message', (msg) => {

      const data: interfaces.ClientLogin.ClientLoginResponse = JSON.parse(msg.toString());

      if (data.Msg === ServerMessages.ClientLoginResponse) {
        expect(data.UUID).toEqual(request.UUID);
        expect(data.UserId).toEqual(request.UserId);
      }

      ws.close();
    })
      .on('close', () => done());

    Services.ClientLogin(ws, request);
  });

});
