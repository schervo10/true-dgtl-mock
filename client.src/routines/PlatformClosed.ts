import readline from 'readline';
import WebSocket from 'ws';
import { ServerMessages } from '../../common/serverMessages';
import * as Services from '../services';

export const PlatformClosed = async (ws: WebSocket) => {

  Services.ClientLogin(ws, {
    Password: 'Testing123',
    UserId: 'MM1_CLOBUI',
  });

  ws.on('message', (msg: string) => {

    try {
      const data = JSON.parse(msg);

      if (data.Msg === ServerMessages.ClientHeartbeatRequest) {
        Services.ClientHeartbeatResponse(ws, data);
        return;
      }

      if (data.Msg === ServerMessages.ClientLogoutResponse) {
        ws.close();
        return;
      }

    } catch (error) {
      console.log(error);
    }

  });
};
