import WebSocket from 'ws';
import { ServerMessages } from '../../common/serverMessages';
import * as Services from '../services';

export const TokenRequest = async (ws: WebSocket) => {

  Services.TokenRequest(ws, {
    SourceIp: '1123.123.123',
  });

  Services.ClientLoginRequest(ws, {
    Secret: '3HawNc7pK8vK3dN+c9GC/Xp24Mo6td1UFFpf7DotQWDOz1F8G5bvTbhUUrMHPYji6GmUg5lTLAD5ro05CVomGQ==',
    Uuid: '36ef4f04-2710-4821-879f-bf5c98254e84',
  });

  ws.on('message', (msg: string) => {

    try {
      const data = JSON.parse(msg);

      if (data.Msg === ServerMessages.ClientHeartbeatRequest) {
        Services.ClientHeartbeatResponse(ws, data);
        return;
      }

      if (data.Msg === ServerMessages.ClientLogoutResponse) {
        ws.close();
        return;
      }

    } catch (error) {
      console.log(error);
    }

  });
};
