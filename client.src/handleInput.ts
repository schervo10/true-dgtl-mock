import readline from 'readline';
import WebSocket from 'ws';
import * as Services from './services';
// import SessionHandler from './session-handler';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const handleInput = (ws: WebSocket) => {
  rl.on('line', (input) => {
    switch (input) {
      case 'close':
          Services.TokenRequest(ws, {
            SourceIp: '1123.123.123',
          });
          break;
      default:
        console.log('Command not recognized');
        break;
    }
  });
};

export default handleInput;
