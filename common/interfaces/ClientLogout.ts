
export interface ClientLogout {
  Msg: 'ClientLogout';
  SeqNum: number;
  Time: number;
  Sender: number;
  UUID: string;
  UserId: string;
  JsonWebToken: string;
}

export interface ClientLogoutResponse {
  Msg: 'ClientLogoutResponse';
  Sender: number;
  Time: number;
  UserId: string;
  ReLogin: boolean;
}
