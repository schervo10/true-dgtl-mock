export interface TokenResponse {
  Msg: string;
  Success: boolean;
  Time: number;
  Token: string;
  Uuid: string;
}

export interface TokenRequest {
  Msg: string;
  RequestPattern: string;
  SourceIp: string;
  Time: number;
  Uuid: string;
}
