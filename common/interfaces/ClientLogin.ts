export interface ClientLogin {
  Msg: string;
  Sender: number;
  UUID?: string;
  Uuid?: string;
  UserId: string;
  Password: string;
}

export interface ClientLoginResponse {
  Msg: string;
  Sender: number;
  UUID?: string;
  Uuid?: string;
  UserId: string;
  JsonWebToken: string;
  PasswordReset: boolean;
  Success?: boolean;
  Time?: number;
}

export interface ClientLoginRequest {
  Msg: string;
  RequestPattern: string;
  Secret: string;
  Sender: number;
  Time: number;
  Uuid: string;
}
