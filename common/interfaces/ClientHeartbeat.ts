export interface ClientHeartbeatRequest {
  Msg: 'ClientHeartbeatRequest';
  SeqNum: number;
  Time: number;
  UUID: string;
  UserId: string;
}

export interface ClientHeartbeatResponse {
  Msg: 'ClientHeartbeatResponse';
  JsonWebToken: string;
  SeqNum: number;
  Time: number;
  UUID: string;
  UserId: string;
}
