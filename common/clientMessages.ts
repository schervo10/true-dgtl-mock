export enum ClientMessages {
  ClientLogin = 'ClientLogin',
  ClientLoginResponse = 'ClientLoginResponse',
  ClientLoginRequest = 'ClientLoginRequest',
  ClientLogout = 'ClientLogout',
  ClientHeartbeatResponse = 'ClientHeartbeatResponse',
  TokenRequest = 'TokenRequest',
}
