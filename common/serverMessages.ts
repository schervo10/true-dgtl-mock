export enum ServerMessages {
  // Heartbeat
  ClientHeartbeatRequest = 'ClientHeartbeatRequest',
  ClientHeartbeatResponse = 'ClientHeartbeatResponse',

  // Login
  ClientLoginRejected = 'ClientLoginRejected',
  ClientLoginResponse = 'ClientLoginResponse',

  // Logout
  ClientLogoutResponse = 'ClientLogoutResponse',
  
  // Token
  TokenResponse = 'TokenResponse',
}
