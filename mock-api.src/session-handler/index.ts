import WebSocket from 'ws';
import * as Interfaces from '../../common/interfaces';
import * as Methods from '../methods/methods';
import { State } from './state';
import getInitialState from './utils/getInitialState';

class SessionHandler {

  public static TokenResponse = (ws: WebSocket, data: Interfaces.Token.TokenRequest) => {
    Methods.TokenResponse(ws, data);
  }

  public ws: WebSocket;
  public UserId: string;
  public JsonWebToken: string;
  public state: State;

  constructor(ws: WebSocket, UserId: string) {
    this.ws = ws;
    this.UserId = UserId;

    this.state = getInitialState();
  }

  public startHeartbeat = (ws: WebSocket) => {

    let seqNum = 0;

    setInterval(() => {
      ws.send(JSON.stringify({
        Msg: 'ClientHeartbeatRequest',
        Sender: 0,
        SeqNum: seqNum,
        Time: 0,
        UUID: 'user1',
        UserId: 'user1',
      }));
      seqNum++;
    }, 2000);
  }

  public ClientHeartbeatResponse = (data: Interfaces.ClientHeartbeat.ClientHeartbeatRequest) => {
    Methods.ClientHeartbeatResponse(this.ws, data);
  }

  public ClientLogin = (data: any) => {
    Methods.ClientLogin(this.ws, data as Interfaces.ClientLogin.ClientLogin).then((token: string) => {
      this.JsonWebToken = token;
    });
  }

  public ClientLoginResponse = (data: any) => {
    Methods.ClientLoginResponse(this.ws, data as Interfaces.ClientLogin.ClientLoginResponse).then((token: string) => {
      this.JsonWebToken = token;
    });
  }

  public ClientLogout = () => {
    Methods.ClientLogout(this.ws, { UserId: this.UserId });
  }

}

export default SessionHandler;
