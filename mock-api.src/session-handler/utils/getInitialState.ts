import { State } from '../state';

const getInitialState = (): State => {
  return {};
};

export default getInitialState;
