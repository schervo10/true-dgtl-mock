import express from 'express';
import http from 'http';
import WebSocket from 'ws';
import { ClientMessages } from '../common/clientMessages';
import { ServerMessages } from '../common/serverMessages';
import gateway from './gateway';
import handleInput from './handleInput';
import SessionHandler from './session-handler';

const PORT = 8080;
const WS_PORT = 9090;

const app = express();
const server = new http.Server(app);

const wss = new WebSocket.Server({ port: WS_PORT });

interface Sessions {
  [id: string]: SessionHandler;
}

wss.on('connection', (ws, req) => {

  const sessions: Sessions = {};

  ws.on('message', (msg) => {
    try {
      const data = JSON.parse(msg.toString());
      if (!data.Msg) {
        throw new Error('Missing Msg property.');
      }

      console.log('Receiving from Client', data.Msg);

      if (data.Msg === ClientMessages.TokenRequest) {
        SessionHandler.TokenResponse(ws, {
          Msg: ServerMessages.TokenResponse,
          RequestPattern: ServerMessages.TokenResponse,
          SourceIp: data.SourceIp,
          Time: 0,
          Uuid: data.Uuid,
        });
        return;
      }

      if (data.Msg === ClientMessages.ClientLogin + 'Request') {
        const key = data.UserId || data.Secret;
        sessions[key] = new SessionHandler(ws, key);
        handleInput(sessions[key]);
        sessions[key].ClientLogin(data);
        sessions[key].startHeartbeat(ws);
        return;
      }

      gateway(sessions[data.UserId], { Msg: data.Msg, data });
    } catch (e) {
      console.log(e);
    }
  });

  ws.on('close', () => {
    console.log('Connection closed');
  });
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

server.listen(PORT, () => {
  console.log('HTTP Server running on Port: ' + PORT);
  console.log('WS Server running on Port: ' + WS_PORT);
});
