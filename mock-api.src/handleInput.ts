import readline from 'readline';
// import WebSocket from 'ws';
// import * as Services from './methods/methods';
import SessionHandler from './session-handler';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const handleInput = (Session: SessionHandler) => {
  rl.on('line', (input) => {
    switch (input) {
      case 'close':
          Session.ClientLogout();
          break;
      default:
        console.log('Command not recognized');
        break;
    }
  });
};

export default handleInput;
