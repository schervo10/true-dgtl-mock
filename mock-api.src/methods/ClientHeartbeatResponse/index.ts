import uuid = require('uuid');
import WebSocket from 'ws';
import * as interfaces from '../../../common/interfaces';
import { ServerMessages } from '../../../common/serverMessages';

export const ClientHeartbeatResponse = (ws: WebSocket, data: interfaces.ClientHeartbeat.ClientHeartbeatRequest) => {

  const response: interfaces.ClientHeartbeat.ClientHeartbeatResponse = {
    JsonWebToken: uuid(),
    Msg: ServerMessages.ClientHeartbeatResponse,
    SeqNum: 1,
    Time: 0,
    UUID: data.UUID,
    UserId: data.UserId,
  };

  console.log('Forwarding Client > ', response.Msg);

  return ws.send(JSON.stringify(response));
};
