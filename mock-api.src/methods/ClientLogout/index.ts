import WebSocket from 'ws';
import * as interfaces from '../../../common/interfaces';
import { ServerMessages } from '../../../common/serverMessages';

interface LogoutRequest {
  UserId: string;
}

export const ClientLogout = (ws: WebSocket, data: LogoutRequest) => {

  const response: interfaces.ClientLogout.ClientLogoutResponse = {
    Msg: ServerMessages.ClientLogoutResponse,
    ReLogin: false,
    Sender: 23,
    Time: 0,
    UserId: data.UserId,
  };

  console.log('Forwarding Client > ', response.Msg);

  return ws.send(JSON.stringify(response));
};
