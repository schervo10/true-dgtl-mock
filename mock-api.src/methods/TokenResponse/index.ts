import WebSocket from 'ws';
import * as interfaces from '../../../common/interfaces';
import { ServerMessages } from '../../../common/serverMessages';

interface TokenRequest {
  SourceIp: string;
  Uuid: string;
}

export const TokenResponse = (ws: WebSocket, data: TokenRequest) => {

  const response: interfaces.Token.TokenResponse = {
    Msg: ServerMessages.TokenResponse,
    Success: true,
    Time: 0,
    Token: '0bc660fe-b6f0-4899-a7b3-3be18ec4a299',
    Uuid: data.Uuid,
  };

  console.log('Forwarding Client > ', response.Msg);

  return ws.send(JSON.stringify(response));
};
