import uuid = require('uuid');
import WebSocket from 'ws';
import * as interfaces from '../../../common/interfaces';
import { ServerMessages } from '../../../common/serverMessages';
import users from './mock';

export const ClientLogin = (ws: WebSocket, data: interfaces.ClientLogin.ClientLogin) =>
  new Promise((resolve, reject) => {
    // const user = users.find((u) => u.UserId === data.UserId);

    // if (!user) {
    //   return ws.send(JSON.stringify({
    //     Message: 'Invalid user',
    //     Msg: ServerMessages.ClientLoginRejected,
    //   }));
    // }

    const JWT = uuid();

    const response: interfaces.ClientLogin.ClientLoginResponse = {
      JsonWebToken: JWT,
      Msg: ServerMessages.ClientLoginResponse,
      PasswordReset: false,
      Sender: 23,
      Success: true,
      UserId: data.UserId,
      Uuid: data.Uuid,
    };

    ws.send(JSON.stringify(response));

    resolve(JWT);
  });
