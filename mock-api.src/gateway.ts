import { ClientMessages } from '../common/clientMessages';
import * as Interfaces from '../common/interfaces';
import SessionHandler from './session-handler';

interface Data {
  Msg: ClientMessages;
  data: any;
}

const gateway = (Session: SessionHandler, data: Data) => {
  switch (data.Msg) {

    case ClientMessages.ClientHeartbeatResponse: {
      // Session.ClientHeartbeatResponse(data.data as Interfaces.ClientHeartbeat.ClientHeartbeatRequest);
      break;
    }

    case ClientMessages.ClientLogout: {
      Session.ClientLogout();
      break;
    }

    case ClientMessages.ClientLoginResponse: {
      Session.ClientLoginResponse(data.data as Interfaces.ClientLogin.ClientLoginResponse);
      break;
    }

    default:
      throw new Error(`Invalid Msg property - ${data.Msg}`);
  }
};

export default gateway;
